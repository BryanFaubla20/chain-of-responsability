﻿using Chain2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bryan.Macias;

namespace Chain2
{
    class Program
    {
        static void Main(string[] args)
        {
            // se definen tres objetos para la cadena
            var comprador = new Comprador();
            var gerente = new Gerente();
            var director = new Director();
           
            // se compone la cadena
            gerente.AgregarSiguiente(director);
            comprador.AgregarSiguiente(gerente);
            // se crea la variable
            var a = new Compra();
            double importe = 1;
            // iniciamos con un ciclo para simular las operaciones consecutivas
            while (importe != 0)
            {
                Console.WriteLine("Ingrese el importe de la compra (0 para terminar)");
                importe = double.Parse(Console.ReadLine());
                // donde se creara el importe de la compra
                a.Importe = importe;
                //se crea un nuevo objeto donde definimos donde quiere comenzar la cadena con su respectiva operacion
                comprador.Procesar(a);
            }
        }
    }
}
